<?php

// Defines
define('FL_CHILD_THEME_DIR', get_stylesheet_directory());
define('FL_CHILD_THEME_URL', get_stylesheet_directory_uri());
define( 'postpercol', '3' );
define( 'productdetail_layout', 'box' );  
$cde = (get_option('CDE_ENV')) && get_option('CDE_ENV')!="" ?get_option('CDE_ENV'):""; 
define('ENV', $cde);
define('GOOGLE_MAP_KEY','AIzaSyD09zQ9PNDNNy9TadMuzRV_UsPUoWKntt8');

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action('wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000);

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script("slick", get_stylesheet_directory_uri()."/resources/slick/slick.min.js", "", "", 1);
    wp_enqueue_script("cookie", get_stylesheet_directory_uri()."/resources/jquery.cookie.js", "", "", 1);
  //  wp_enqueue_style('custom-style', get_stylesheet_directory_uri().'/resources/pluginstyles.min.css', "", "", 1 );

    wp_enqueue_script('script-min-js',get_stylesheet_directory_uri().'/resources/script.min.js', "", "", 1);
    wp_enqueue_style('font-awesome-styles', get_stylesheet_directory_uri().'/resources/font-awesome.min.css', "", "", 1);
   
    //wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});


//Facet Title Hook
add_filter('facetwp_shortcode_html', function ($output, $atts) {
    if (isset($atts['facet'])) {
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2);

add_filter('wp_nav_menu_items', 'do_shortcode');

// Register menus
function register_my_menus()
{
    register_nav_menus(
        array(
            'footer-1' => __('Footer Menu 1'),
            'footer-2' => __('Footer Menu 2'),
            'footer-3' => __('Footer Menu 3'),
            'footer-4' => __('Footer Menu 4'),
            'footer-5' => __('Footer Menu 5'),
            'site-map' => __('Site Map'),
        )
    );
}
add_action('init', 'register_my_menus');

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js", array(), '3.2.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );
 
// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');


function register_shortcodes()
{
    $dir=dirname(__FILE__)."/shortcodes";
    $files=scandir($dir);
    foreach ($files as $k=>$v) {
        $file=$dir."/".$v;
        if (strstr($file, ".php")) {
            $shortcode=substr($v, 0, -4);
            add_shortcode($shortcode, function ($attr, $content, $tag) {
                ob_start();
                include(dirname(__FILE__)."/shortcodes/".$tag.".php");
                $c=ob_get_clean();
                return $c;
            });
        }
    }
}



// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id=0, $size="", $url=false, $attr="")
{

    //Show a theme image
    if (!is_numeric($id) && is_string($id)) {
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if (file_exists(to_path($img))) {
            if ($url) {
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if (!$id) {
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if (is_object($id)) {
        if (!empty($id->ID)) {
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if (get_post_type($id)!="attachment") {
        $id=get_post_thumbnail_id($id);
    }

    if ($id) {
        $image_url=wp_get_attachment_image_url($id, $size);
        if (!$url) {
            //If image is a SVG embed the contents so we can change the color dinamically
            if (substr($image_url, -4, 4)==".svg") {
                $image_url=str_replace(get_bloginfo("url"), ABSPATH."/", $image_url);
                $data=file_get_contents($image_url);
                echo strstr($data, "<svg ");
            } else {
                return wp_get_attachment_image($id, $size, 0, $attr);
            }
        } elseif ($url) {
            return $image_url;
        }
    }
}
 
// shortcode to show H1 google keyword fields
function new_google_keyword()
{
    if (@$_COOKIE['keyword'] ==""  && @$_COOKIE['brand'] == "") {
        return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring<h1>';
    } else {
        $keyword = $_COOKIE['keyword'];
        $brand = $_COOKIE['brand'];
        return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on '.$brand.' '.$keyword.'<h1>';
    }
}
  add_shortcode('google_keyword_code', 'new_google_keyword');
  add_action('wp_head', 'cookie_gravityform_js');

  function cookie_gravityform_js()
  { // break out of php?>
  <script>
	  var brand_val ='<?php echo $_COOKIE['brand']; ?>';
	  var keyword_val = '<?php echo $_COOKIE['keyword']; ?>';  

      jQuery(document).ready(function($) {
      jQuery("#input_14_9").val(keyword_val);
      jQuery("#input_14_10").val(brand_val);
    });
  </script>
  <?php
     setcookie('keyword', '', -3600);
      setcookie('brand', '', -3600);
  }

// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head()
{
    ?>
      <style>
          .googlekeyword {
             text-align:center;
             color: #fff;
             text-transform: capitalize;   
              /* font-size:2.5em !important; */
              font-size: 36px !important;
           }
      </style>  
   <?php
}
/**
 * Dequeue the jQuery UI styles.
 *
 * Hooked to the wp_print_styles action, with a late priority (100),
 * so that it is after the style was enqueued.
 */
function remove_pagelist_css() {
    wp_dequeue_style( 'page-list-style' );
 }
 add_action( 'wp_print_styles', 'remove_pagelist_css', 100 );


 
function formatPhoneNumber($phoneNumber)
{
    $phoneNumber = preg_replace('/[^0-9]/', '', $phoneNumber);

    if (strlen($phoneNumber) > 10) {
        $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
        $areaCode = substr($phoneNumber, -10, 3);
        $nextThree = substr($phoneNumber, -7, 3);
        $lastFour = substr($phoneNumber, -4, 4);

        $phoneNumber = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
    } elseif (strlen($phoneNumber) == 10) {
        $areaCode = substr($phoneNumber, 0, 3);
        $nextThree = substr($phoneNumber, 3, 3);
        $lastFour = substr($phoneNumber, 6, 4);

        $phoneNumber = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
    } elseif (strlen($phoneNumber) == 7) {
        $nextThree = substr($phoneNumber, 0, 3);
        $lastFour = substr($phoneNumber, 3, 4);

        $phoneNumber = $nextThree.'-'.$lastFour;
    }

    return $phoneNumber;
}
 
function storelocation_address($arg)
{
    
    $website = json_decode(get_option('website_json'));
    $weekdays = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
    $locations = "<ul class='storename'><li>";
    
    
    for ($i=0;$i<count($website->locations);$i++) {
        $location_name = isset($website->locations[$i]->name)?$website->locations[$i]->name:"";

        if($website->locations[$i]->type == 'store'){

            if (in_array(trim($location_name), $arg)){

                if($website->locations[$i]->name == 'Main' || $website->locations[$i]->name=="MAIN"){

                    $location_name = get_bloginfo( 'name' );
                    
                }else{
                  
                    $location_name =  isset($website->locations[$i]->name)?$website->locations[$i]->name." ":"";

                }


                $location_address_url =  $location_name;
                $location_address_url  .= isset($website->locations[$i]->address)?$website->locations[$i]->address." ":"";
                $location_address_url .= isset($website->locations[$i]->city)?$website->locations[$i]->city." ":"";
                $location_address_url .= isset($website->locations[$i]->state)?$website->locations[$i]->state." ":"";
                $location_address_url .= isset($website->locations[$i]->postalCode)?$website->locations[$i]->postalCode." ":"";
        
                $location_address  = isset($website->locations[$i]->address)?"<p>".$website->locations[$i]->address."</p>":"";
                $location_address .= isset($website->locations[$i]->city)?"<p>".$website->locations[$i]->city.", ":"<p>";
                $location_address .= isset($website->locations[$i]->state)?$website->locations[$i]->state:"";
                $location_address .= isset($website->locations[$i]->postalCode)?" ".$website->locations[$i]->postalCode."</p>":"</p>";
                
                $location_phone  = "";
                if (isset($website->locations[$i]->phone)) {
                    $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->phone);
                    $location_phone  = formatPhoneNumber($website->locations[$i]->phone);
                }
                if (isset($website->locations[$i]->forwardingPhone) && $website->locations[$i]->forwardingPhone != "") {
                    $forwarding_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->forwardingPhone);
                    $forwarding_phone  = formatPhoneNumber($website->locations[$i]->forwardingPhone);
                    
                }
                else{
        
                    $forwarding_tel ="#";
                    $forwarding_phone  = formatPhoneNumber(8888888888);
                }


                if (in_array("loc", $arg)) {

                    if (in_array('nolink', $arg)){
                        $locations .= '<div class="store-container">';
                        //$locations .= '<div class="name">'.$location_name.'</div>';
                        $locations .= '<div class="address">'.$location_address.'</div>';
                        //$locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                        $locations .= '</div>';
                    }else{

                        $locations .= '<div class="store-container">';
                        //$locations .= '<div class="name">'.$location_name.'</div>';
                        $locations .= '<div class="address"><a href="https://maps.google.com/maps/?q='.urlencode($location_address_url).'" target="_blank" '.'>'.$location_address.'</a></div>';
                        //$locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                        $locations .= '</div>';

                    }
                        
                    
                }
                if (in_array("forwardingphone", $arg)) {

                        if (in_array('nolink', $arg)){
                            $locations .= "<div class='phone'><span>".$forwarding_phone."</span></div>";
                        }
                        else{
                            $locations .= "<div class='phone'><a href='tel:".$forwarding_tel."'><span>".$forwarding_phone."</span></a></div>";
                        }

                }
                if (in_array("ohrs", $arg)) {
                        $locations .= '<div class="store-opening-hrs-container">';
                        $locations .= '<ul class="store-opening-hrs">';
                            
                        for ($j = 0; $j < count($weekdays); $j++) {
                          //  $locations .= $website->locations[$i]->monday;
                            if (isset($website->locations[$i]->{$weekdays[$j]})) {
                                $locations .= '<li>'.ucfirst($weekdays[$j]).' : <span>'.$website->locations[$i]->{$weekdays[$j]}.'</span></li>';
                            }
                        }
                        $locations .= '</ul>';
                        $locations .= '</div>';
                }
                if (in_array("dir", $arg)) {
                    
                        $locations .= '<div class="direction">';
                        $locations .= '<a href="https://maps.google.com/maps/?q='.urlencode($location_address_url).'" target="_blank" class="fl-button" role="button">
                                            <span class="button-text">GET DIRECTIONS</span></a>';
                        $locations .= '</div>';
                    
                }
                if (in_array("map", $arg)) {
                    
                        $locations .= '<div class="map-container">
                        <iframe src="https://www.google.com/maps/embed/v1/place?key='.GOOGLE_MAP_KEY.'&amp;q='.urlencode($location_address_url).'"&zoom=nn style="width:100%;min-height:450px;border:0px;"></iframe>
                        </div>';
                    
                }
                if (in_array("phone", $arg)) {
                            $locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                }
            } 

        }        
    }
$locations .= '</li>';
$locations .= '</ul>';
    
    return $locations;
}
 add_shortcode('storelocation_address', 'storelocation_address');


 

function contact_phonenumber($atts)
{
    if (get_option('api_contact_phone')) {
        return '<a href="tel:'.get_option('api_contact_phone').'" target="_self" class="fl-icon-text-link fl-icon-text-wrap">'.get_option('api_contact_phone').'</a>';
    } else {
        return '<a href="javascript:void(0)" target="_self" class="fl-icon-text-link fl-icon-text-wrap">(XXX) (XXXXXXX)</a>';
    }
}
add_shortcode('contactPhnumber', 'contact_phonenumber');


function getSocailIcons()
{
    $details = json_decode(get_option('social_links'));
    $return  = '';
    if (isset($details)) {
        $return  = '<ul class="social-icons">';
        foreach ($details as $key => $value) {
            if ($value->platform !="googleBusiness") {
              
            if ($value->platform=="linkedIn") {
                $value->platform = "linkedin";
            }
            
                
                
            if ($value->active) {
                $return  .= '<li><a href="'.$value->url.'" target="_blank" title="'.$value->platform.'"><i class="fab fa-'.strtolower($value->platform).'"></i></a></li>';
            }
        }
        }
        $return  .= '</ul>';

    }
    return $return;
}
add_shortcode('getSocailIcons', 'getSocailIcons');

function contactInformation($atts)
{
    $contacts = json_decode(get_option('website_json'));
    $info="";
    if (is_array($contacts->contacts)) {
        if (in_array("withicon", $atts)) {
            for ($i=0;$i<count($contacts->contacts);$i++) {
                if (in_array($contacts->contacts[$i]->type, $atts)) {
                    if (in_array("email", $atts)) {
                        $info  .= "<a class='mail-icon icon-link' href='mailto:".$contacts->contacts[$i]->email."'>".$contacts->contacts[$i]->email."</a>";
                    }
                    if (in_array("phone", $atts)) {
                        $info  .= "<a class='phone-icon icon-link' href='tel:".preg_replace('/[^0-9]/', '', $contacts->contacts[$i]->phone)."'>".formatPhoneNumber($contacts->contacts[$i]->phone)."</a>";
                    }
                    if (in_array("notes", $atts)) {
                        $info  .=  $contacts->contacts[$i]->notes;
                    }
                }
            }
        } else {
            for ($i=0;$i<count($contacts->contacts);$i++) {
                if (in_array($contacts->contacts[$i]->type, $atts)) {
                    if (in_array("email", $atts)) {
                        $info  .= "<a class='mail-icon-with icon-link-with' href='mailto:".$contacts->contacts[$i]->email."'>".$contacts->contacts[$i]->email."</a>";
                    }
                    if (in_array("phone", $atts)) {
                        $info  .= "<a class='phone-icon-with icon-link-with' href='tel:".preg_replace('/[^0-9]/', '', $contacts->contacts[$i]->phone)."'>".formatPhoneNumber($contacts->contacts[$i]->phone)."</a>";
                    }
                    if (in_array("notes", $atts)) {
                        $info  .=  $contacts->contacts[$i]->notes;
                    }
                }
            }
        }
    }
    return $info;
}
add_shortcode('contactInformation', 'contactInformation');


function locationInformation($atts)
{
    $website = json_decode(get_option('website_json'));
    
    for ($i=0;$i<count($website->locations);$i++) {

        if($website->locations[$i]->type == 'store'){

        if (in_array($website->locations[$i]->name, $atts)) {
            if (in_array("license", $atts)) {
                $info  .= "<div class='store-license'>".$website->locations[$i]->licenseNumber."</div>";
            }
            if (in_array("phone", $atts)) {
                $info  .= "<a class='phone-icon-with icon-link-with' href='tel:".preg_replace('/[^0-9]/', '', $website->locations[$i]->phone)."'>".formatPhoneNumber($website->locations[$i]->phone)."</a>";
            }
        }
    }
    }
    return $info;
}
add_shortcode('locationInformation', 'locationInformation');


function hookSharpStrings() {

    $website_json =  json_decode(get_option('website_json'));
    $sharpspringDomainId =$sharpspringTrackingId ="";
    for($j=0;$j<count($website_json->sites);$j++){
        
        if($website_json->sites[$j]->instance == ENV){
           
                $sharpspringTrackingId = $website_json->sites[$j]->sharpspringTrackingId;
                $sharpspringDomainId = $website_json->sites[$j]->sharpspringDomainId;
           
            }
        }
        
       if(isset($sharpspringTrackingId) && trim($sharpspringTrackingId) !="" ){
   
    ?>
           <script type="text/javascript">

                    var _ss = _ss || [];

                    _ss.push(['_setDomain', 'https://<?php echo $sharpspringDomainId;?>.marketingautomation.services/net']);

                    _ss.push(['_setAccount', '<?php echo $sharpspringTrackingId;?>']);

                    _ss.push(['_trackPageView']);

                    (function() {

                    var ss = document.createElement('script');

                    ss.type = 'text/javascript'; ss.async = true;

                    ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + '<?php echo $sharpspringDomainId;?>.marketingautomation.services/client/ss.js?ver=1.1.1';

                    var scr = document.getElementsByTagName('script')[0];

                    scr.parentNode.insertBefore(ss, scr);

                    })();

                    </script>
    <?php
       }
}
add_action('wp_head', 'hookSharpStrings');

function getRetailerInfo($arg){


    $website = json_decode(get_option('website_json'));
    if (in_array('city', $arg)){
        $info =  isset($website->locations[0]->city)?"<span class='city retailer'>".$website->locations[0]->city."</span>":"";
    }
    else if (in_array('state', $arg)){
        $info =  isset($website->locations[0]->state)?"<span class='state retailer'>".$website->locations[0]->state."</span>":"";
    }
    else if (in_array('zipcode', $arg)){
        $info =  isset($website->locations[0]->postalCode)?"<span class='zipcode retailer'>".$website->locations[0]->postalCode."</span>":"";
    }
    else if (in_array('legalname', $arg)){
        $info =  isset($website->name)?"<span class='name retailer'>".$website->name."</span>":"";
    }
    else if (in_array('address', $arg)){
        $info =  isset($website->locations[0]->address)?"<span class='street_address retailer'>".$website->locations[0]->address."</span>":"";
    }
    else if (in_array('phone', $arg)){
        if (isset($website->locations[0]->phone)) {
            $location_tel = preg_replace('/[^0-9]/', '', $website->locations[0]->phone);
            $location_phone  = formatPhoneNumber($website->locations[0]->phone);
        }
        if (in_array('nolink', $arg)){
            $info =  isset($website->locations[0]->phone)?"<span class='phone retailer'>".$location_phone."</span>":""; 
        }
        else{
            
            $info =  isset($website->locations[0]->phone)?"<a href='tel:".$location_tel."' class='phone retailer'>"."<span>".$location_phone."</span></a>":"";
        }
    }
    else if (in_array('forwarding_phone', $arg)){
        if (isset($website->locations[0]->forwardingPhone)) {
            $location_tel = preg_replace('/[^0-9]/', '', $website->locations[0]->forwardingPhone);
            $location_phone  = formatPhoneNumber($website->locations[0]->forwardingPhone);
        }
        if (in_array('nolink', $arg)){
            $info =  isset($website->locations[0]->forwardingPhone)?"<span class='phone retailer'>".$location_phone."</span>":""; 
        }
        else{
            
            $info =  isset($website->locations[0]->forwardingPhone)?"<a href='tel:".$location_tel."' class='phone retailer' >"."<span>".$location_phone."</span></a>":"";
        }
    }
    else if (in_array('companyname', $arg)){
        for($j=0;$j<count($website->sites);$j++){
        
            if($website->sites[$j]->instance == ENV){
                    $info =  isset($website->sites[$j]->name)?"".$website->sites[$j]->name."":"";
                    break;
                }
            }
    }
    else if (in_array('site_url', $arg)){
        for($j=0;$j<count($website->sites);$j++){
        
            if($website->sites[$j]->instance == ENV){
                    $info =  isset($website->sites[$j]->url)?"<span class='site_url retailer'>".$website->sites[$j]->url."</span>":"";
                    break;
                }
            }
    }
    
    return $info;  
}

add_shortcode('Retailer', 'getRetailerInfo');


function updateSaleCoupanInformation($arg){
    $promos = json_decode(get_option('promos_json'));

    $saleinformation = json_decode(get_option('saleinformation'));
    $saleinformation = (array)$saleinformation;

    $salespriority = json_decode(get_option('salespriority'));
    $salespriority = (array)$salespriority;
    $high_priprity = min($salespriority);


    if(isset($_GET['promo']) && !empty($_GET['promo'])) {

        $promo_code = $_GET['promo'];

    }else{ 

        $promo_code = array_search($high_priprity,$salespriority);
    }

    
   $div='';

   $website_json =  json_decode(get_option('website_json'));
   $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
   $loc_country = $website_json->locations[0]->country;
    if($loc_country =='US'){ $en = 'en_us'; }elseif($loc_country =='CA'){ $en = 'en_ca'; }
    
    
    $seleinformation = json_decode(get_option('saleinformation'));
    //update_option('saleinformation',json_encode($seleinformation));
    $seleinformation = (object)$seleinformation;

    $salebannerdata = json_decode(get_option('salebannerdata'));
    
        if(in_array('rugshop',$arg) && in_array('salebanner',$arg)){


            foreach($salebannerdata as $bannerinfo){
                
           
                if($bannerinfo->size == 'fullwidth' && $bannerinfo->rugShop == 'true'){  

                    $banner_img_deskop = $bannerinfo->images->desktop;
                    $banner_img_mobile = $bannerinfo->images->mobile;

                    if($bannerinfo->link == ''){

                        $banner_link = 'https://rugs.shop/'.$en.'/?store='.$rugAffiliateCode;

                    }else{

                        $banner_link = 'https://rugs.shop/'.$en.'/'.$bannerinfo->link.'?store='.$rugAffiliateCode;
                    }
                    
                }
            }

            $div = "<div class='salebanner banner-deskop fullwidth rugshopbanner'><a target='_blank' href='".$banner_link."'><img srcset='https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$banner_img_deskop." 4025w, 
            https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$banner_img_deskop." 3019w, 
            https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop." 2013w, 
            https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop." 1006w' 
            
            src='https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop."'  style='width:100%;text-align:center' alt='sales' />
            </a></div>";       
            $div .= "<div class='salebanner banner-mobile rugshopbanner-mobile'><a target='_blank' href='".$banner_link."'><img src='".$banner_img_mobile."' alt='sales' /></a></div>";
         
        }else{        
                  
      
       if(in_array('full',$arg) || in_array('salebanner',$arg)  ){  
         
        if($salebannerdata != ''){
       
            foreach($salebannerdata as $bannerinfo){
                
           
                if($bannerinfo->size == 'fullwidth' && $bannerinfo->priority == $high_priprity ){  

                    $banner_img_deskop = $bannerinfo->images->desktop;
                    $banner_img_mobile = $bannerinfo->images->mobile;
                    $banner_link = $bannerinfo->link;
                }
            }

            $div = "<div class='salebanner banner-deskop fullwidth'><a href='".$banner_link."'><img srcset='https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$banner_img_deskop." 4025w, 
            https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$banner_img_deskop." 3019w, 
            https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop." 2013w, 
            https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop." 1006w' 
            
            src='https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop."'  style='width:100%;text-align:center' alt='sales' />
            </a></div>";

       
        $div .= "<div class='salebanner banner-mobile'><a href='".$banner_link."'><img src='".$banner_img_mobile."' alt='sales' /></a></div>";
        }
          }
          if(in_array('fixed',$arg) && in_array('salebanner',$arg)){     
            if($salebannerdata != ''){  
           
            foreach($salebannerdata as $bannerinfo){

                if($bannerinfo->size == 'fixedwidth' && $bannerinfo->priority == $high_priprity){

                    $banner_img_deskop =  $bannerinfo->images->desktop;
                    $banner_img_mobile = $bannerinfo->images->mobile;
                    $banner_link = $bannerinfo->link;
                }
            }
        $div = "<div class='salebanner banner-deskop fixedwidth' style='text-align:center'><a href='".$banner_link."'><img src='https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop."'  alt='sales' />
            </a></div>";

       
        $div .= "<div class='salebanner banner-mobile'><a href='".$banner_link."'><img src='".$banner_img_mobile."' alt='sales' /></a></div>";
          }
          }
        

        }
    if(in_array('categorybanner',$arg)){
     
        if(isset($seleinformation->category_banner_img_deskop)&& $seleinformation->category_banner_img_deskop != ""){ 
        $div = "<div class='salebanner ts banner-deskop'><a href='".$seleinformation->banner_link."'><img src='".$seleinformation->category_banner_img_deskop."' alt='sales' /></a></div>";        
        }
        if(isset($seleinformation->category_banner_img_mobile)&& $seleinformation->category_banner_img_mobile != "")
        $div .= "<div class='salebanner banner-mobile'><a href='".$seleinformation->banner_link."'><img src='".$seleinformation->category_banner_img_mobile."' alt='sales' /></a></div>";
    }
     if(in_array('heading',$arg)){
        $price = isset($seleinformation->$promo_code->saveupto_doller)?$seleinformation->$promo_code->saveupto_doller:"";
        if (@$_COOKIE['keyword'] ==""  && @$_COOKIE['brand'] == "") {
            
            $div ='<h1 class="googlekeyword">'.$price.'</h1>';
        } else {
            $keyword = $_COOKIE['keyword'];
            $brand = $_COOKIE['brand'];
            $div = '<h1 class="googlekeyword">'.$price.' on '.$brand.' '.$keyword.'<h1>';
        }
    }
     if(in_array('content',$arg)){
        
         $string = isset($seleinformation->$promo_code->content)?$seleinformation->$promo_code->content:"";
         $div = '<div class="form-content">'.$string.'</div>';
    }
    if(in_array('image_onform',$arg) && isset($seleinformation->$promo_code->image_onform)){
        
        if(in_array('backgrondimage',$arg) || in_array('background_img',$arg)){

            $div = "<div class='salebanner floor-coupon-img  banner-deskop'  id='backgrondimage' ><img src='".$seleinformation->$promo_code->image_onform."' alt='sales' /></div>";
            $div .= "<div class='salebanner floor-coupon-img  banner-mobile'  id='backgrondimage' ><img src='".$seleinformation->$promo_code->image_onform_mobile."' alt='sales' /></div>";
        }
        else{
            $div = "<div class='salebanner floor-coupon-img  banner-deskop'><img src='".$seleinformation->$promo_code->image_onform."' alt='sales' /></div>";
            $div .= "<div class='salebanner floor-coupon-img  banner-mobile'><img src='".$seleinformation->$promo_code->image_onform_mobile."' alt='sales' /></div>";
        }
    }
    if(in_array('message',$arg) && isset($seleinformation->$promo_code->message)){
        
        $div = "<div class='message'>".$seleinformation->$promo_code->message."</div>";
       
    }
    if(in_array('navigation',$arg) && isset($seleinformation->navigation_img)){
        if (in_array('image', $arg)) {
            $div = "<div class='navigation_img'><a href='".$seleinformation->navigation_link."'><img src='".$seleinformation->navigation_img."' alt='sales' /></a></div>";
        }
        else if(in_array('text',$arg)){
            $div = "<div class='navigation_img'><a href='".$seleinformation->navigation_link."'>'".$seleinformation->navigation_text."</a></div>";
        }
       
    }
    if (in_array('homepage_banner', $arg)) {
        if (isset($seleinformation->slider) && count($seleinformation->slider) > 0) {
            foreach ($seleinformation->slider as $slide) {
                if (in_array('rugshop', $arg)) {

                    $div = "<div class='rugshop floor-coupon-img' id='homepage-banner-bg' data-bg='".$seleinformation->background_image."'><a href='https://rugs.shop/$en/?store=".$rugAffiliateCode."' target='_blank'><img class='salebanner-slide' src='".$slide->slider_coupan_img."' alt='sales' /></a></div>";
                    break;
                }
                else{
    
                    $div = "<div class='coupon floor-coupon-img' id='homepage-banner-bg' data-bg='".$seleinformation->background_image."'><a href='/flooring-coupon/'><img class='salebanner-slide' src='".$slide->slider_coupan_img."' alt='sales' /></a></div>";
                    break;
                }
            }
        }
    }
	if (in_array('banner', $arg)) {
        if (isset($seleinformation->$promo_code->slider) && count($seleinformation->$promo_code->slider) > 0) {
            foreach ($seleinformation->$promo_code->slider as $slide) {
                $div = "<div class='banner-image' style=''><a href='".$slide->link."'><img class='salebanner-slide' src='".$slide->slider_coupan_img."' alt='sales' /></a></div>";
				break;
            }
        }
    }
    
    
    if (in_array('print_coupon', $arg)) {
        $salebannerdata = json_decode(get_option('salebannerdata'));
        if(is_array($promos) && count($promos) > 0){
            foreach($promos as $promo){
                if($promo->formName == "Shaw Coupon"){
                    if(is_array($promo->widgets) && count($promo->widgets) > 0){
                        foreach($promo->widgets as $widget){
                            if(is_array($widget->images) && count($widget->images) > 0){
                                foreach($widget->images as $image){
                                    if($image->type == "print" ){
                                        
                                        if(strpos($image->url, "http") == false){
                                            $img = "https://".$image->url;
                                        }else{
                                            $img =$image->url;
                                        }
                                        $div = "<a href='".$img."' target='_blank' class='print_coupon_btn btn fl-button'>PRINT COUPON</a>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }       
    }

    if (in_array('popup_img', $arg)) {
        $salebannerdata = json_decode(get_option('salebannerdata'));
        if(is_array($promos) && count($promos) > 0){
            foreach($promos as $promo){
                if($promo->formName == "Shaw Coupon"){
                    if(is_array($promo->widgets) && count($promo->widgets) > 0){
                        foreach($promo->widgets as $widget){
                            if($widget->type="popup" && is_array($widget->images) && count($widget->images) > 0){
                                foreach($widget->images as $image){
                                    if($image->type == "graphic" ){
                                        if(strpos($image->url, "http") == false){
                                            $img = "https://".$image->url;
                                        }else{
                                            $img =$image->url;
                                        }
                                        $div = "<a href='/flooring-coupon/' class='popup_img_link'><img class='popup_img' src='".$img."' alt='Shaw Popup Image' width='850' height='315'></a>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }       
    }
    
    return $div;
}

add_shortcode('coupon', 'updateSaleCoupanInformation');

function copyrightstext($arg)
{
    $result = "<p>Copyright ©".do_shortcode('[fl_year]')." ".get_bloginfo().". All Rights Reserved.</p>";
    return $result;
}

add_shortcode('copyrights', 'copyrightstext');


//Colorwall Home Page Banner Shortcode

function colorwall_home_banner($arg)
{
    $product_json =  json_decode(get_option('product_json')); 
 
    foreach ($product_json as $data) {
        foreach ($data as $key => $value) {
           if( $key=="manufacturer" &&   $value== "coretec")
               $isCoretec=true;
        }     
    }
    $coretec_colorwall =  get_option('coretec_colorwall');
    if($coretec_colorwall == '1' && $isCoretec){

       $content = do_shortcode('[fl_builder_insert_layout slug="colorwall-mobile"]');
       $content .=  do_shortcode('[fl_builder_insert_layout slug="colorwall-desktop-banner"]');

       return $content;

    }
}

add_shortcode('colorwallhomebanner', 'colorwall_home_banner');


//Yoast SEO Breadcrumb addded
function bbtheme_yoast_breadcrumbs() {
    if ( function_exists('yoast_breadcrumb') && ! is_front_page() ) {
        yoast_breadcrumb('<div id="breadcrumbs"><div class="container">','</div></div>');
    }
}
add_action( 'fl_content_open', 'bbtheme_yoast_breadcrumbs' );

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail' );

function wpse_100012_override_yoast_breadcrumb_trail( $links ) {
    global $post;

    if ( is_singular( 'hardwood_catalog' )  ) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );

    }elseif (is_singular( 'carpeting' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpet/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );

    }elseif (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl/',
            'text' => 'Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }elseif (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }elseif (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/tile/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }elseif (is_singular( 'solid_wpc_waterproof' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/waterproof-flooring/',
            'text' => 'Waterproof Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/waterproof-flooring/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
    }

    return $links;
}


  //This filter is executed before displaying each field and can be used to dynamically populate fields with a default value.
  
  add_filter( 'gform_field_value_salepromocode', 'populate_salepromocode' );
  function populate_salepromocode( $value ) {    

    $saleinformation = json_decode(get_option('saleinformation'));
    $saleinformation = (array)$saleinformation;

    $salespriority = json_decode(get_option('salespriority'));
    $salespriority = (array)$salespriority;
    $high_priprity = min($salespriority);

    if (isset($_GET['promo']) && $_GET['promo']!='') {

     return $_GET['promo'];
     
    }else{
        
        foreach($saleinformation as $promo){      
    
            if($promo->priority == $high_priprity){
               
                  return $promo->promoCode;
            }
      
          }
    }
  } 

  add_filter( 'gform_field_value_saleformtype', 'populate_saleformtype' );
  function populate_saleformtype( $value ) {
    $saleinformation = json_decode(get_option('saleinformation'));
    $saleinformation = (array)$saleinformation;

    $salespriority = json_decode(get_option('salespriority'));
    $salespriority = (array)$salespriority;
    $high_priprity = min($salespriority);

    foreach( $saleinformation as $sale){
    
        if (isset($_GET['promo'])) {
                if( $sale->promoCode == $_GET['promo']){          

                    return $sale->formType;
                    
                }
        }else{

            if($sale->priority == $high_priprity){
               
                return $sale->formType;
           }

        }
    }
     
  } 

// Add Yoast SEO sitemap to virtual robots.txt file
function surbma_yoast_seo_sitemap_to_robotstxt_function( $output ) {

    $homeURL = get_home_url();
  $output = '';
      $output .= 'User-agent: *' . PHP_EOL;
      if(get_option('blog_public')=='0'){
      $output .= 'Disallow: /' . PHP_EOL;
      }else{
          $output .= 'Disallow: /wp-admin/' . PHP_EOL;
          $output .= "Allow: /wp-admin/admin-ajax.php". PHP_EOL;
          $output .= "Sitemap: $homeURL/sitemap_index.xml".PHP_EOL;
      }
      

return $output;
}
add_filter( 'robots_txt', 'surbma_yoast_seo_sitemap_to_robotstxt_function', 10, 1 );

//Floorte Home Page Banner Shortcode

function floorte_home_banner($arg)
{   
       $content = do_shortcode('[fl_builder_insert_layout slug="floorte-mobile"]');
       $content .=  do_shortcode('[fl_builder_insert_layout slug="floorte-desktop"]');

       return $content;
}

add_shortcode('floortehomebanner', 'floorte_home_banner');


function featured_products_slider($atts) {

    // print_r($atts);
 
     $args = array(
         'post_type' => $atts['0'],        
         'posts_per_page' => 8,
         'orderby' => 'rand',
         'meta_query' => array(
             array(
                 'key' => 'collection', 
                 'value' => $atts['1'], 
                 'compare' => '=='
             )
         ) 
     );
     $query = new WP_Query( $args );
     if( $query->have_posts() ){
         $outpout = '<div class="featured-products"><div class="featured-product-list">';
         while ($query->have_posts()) : $query->the_post(); 
             $gallery_images = get_field('gallery_room_images');
             $gallery_img = explode("|",$gallery_images);
            
             // loop through the rows of data

             $room_image =  single_gallery_image_product(get_the_ID(),'600','400');
            
 
             $color_name = get_field('color');
             $brand_name = get_field('brand');
             $collection = get_field('collection');
             $post_type = get_post_type_object(get_post_type( get_the_ID() ));
            
             
             $outpout .= '<div class="featured-product-item">
                 <div class="featured-inner">
                     <div class="prod-img-wrap">
                         <img src="'.$room_image.'" alt="'.get_the_title().'" />
                         <div class="button-wrapper">
                             <div class="button-wrapper-inner">
                                 <a href="'.get_the_permalink().'" class="button alt viewproduct-btn">View Product</a>';
              
            if( get_option('getcouponbtn') == 1){

                $outpout .= '<a href="/flooring-coupon/" class="button fea-coupon_btn">Get Coupon</a>';
            }
              
              $outpout .= '</div>
                         </div>
                     </div>
                     <div class="product-info">
                         <div class="slider-product-title"><a href="'.get_the_permalink().'">'.$collection.'</a></div>
                         <h3 class="floorte-color">'.$color_name.'</h3>
                     </div>
                 </div>
             </div>';
 
         endwhile;
         $outpout .= '</div></div>';
         wp_reset_query();
     }  
 
 
     return $outpout;
 }
 add_shortcode( 'featured_products_slider', 'featured_products_slider' );

 //Brand Banner header Shortcode

function brand_header_banner_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/floorte-header.jpg" alt="Floorte waterproof hardwood flooring for home | '.$storename.'" title="" itemprop="image"  />';
    }  
       return $content;  
}

add_shortcode('brand_header_banner', 'brand_header_banner_function');

//Floorte Shortcode for alt 
function brand_roomscene_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/roomscenes.jpg" alt="Floorte waterproof hardwood flooring for your home | '.$storename.'" title="" itemprop="image"  />';
    }  
       return $content;  
}

add_shortcode('brand_roomscene', 'brand_roomscene_function');

//Floorte Shortcode for alt 
function brand_contact_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/Floorte-Hardwood-2-tier-Rendering_FINAL.jpg" alt="Floorte waterproof hardwood flooring display | '.$storename.'" title="" itemprop="image"  />';
    }  
       return $content;  
}

add_shortcode('brand_contact', 'brand_contact_function');

//Floorte Shortcode for alt 
function brand_construction_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/Group-6.jpg" alt="Floorte waterproof hardwood flooring construction | '.$storename.'" title="" itemprop="image"  />';
    }
          return $content;  
}

//Floorte Shortcode for alt 
add_shortcode('brand_construction', 'brand_construction_function');

function brand_construction_mobile_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/floorte-layers-mobile.jpg" alt="Floorte waterproof hardwood flooring construction | '.$storename.'" title="" itemprop="image"  />';
    }  

       return $content;  
}

add_shortcode('brand_construction_mobile', 'brand_construction_mobile_function');

function brand_resistant_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/floorte-scuf-pic.jpg" alt="Floorte waterproof hardwood flooring with stain resistant finish | '.$storename.'" title="" itemprop="image"  />';
    }   
       return $content;  
}

add_shortcode('brand_resistant', 'brand_resistant_function');


//accessiBe script hook with cde parameter
$website_json_data = json_decode(get_option('website_json'));

foreach($website_json_data->sites as $site_cloud){
            
    if($site_cloud->instance == 'prod'){

        if( $site_cloud->accessibe == 'true'){

            add_action('fl_body_close', 'accessiBe_custom_footer_js');

        }

    }
}

function accessiBe_custom_footer_js() {
    
  echo "<script > (function(document, tag) {
    var script = document.createElement(tag);
    var element = document.getElementsByTagName('body')[0];
    script.src = 'https://acsbap.com/api/app/assets/js/acsb.js';
    script.async = true;
    script.defer = true;
    (typeof element === 'undefined' ? document.getElementsByTagName('html')[0] : element).appendChild(script);
    script.onload = function() {
        acsbJS.init({
            statementLink: '',
            feedbackLink: '',
            footerHtml: 'Enable powered by <a href=https://enablemysite.com/ target=_blank>enablemysite</a>',
            hideMobile: false,
            hideTrigger: false,
            language: 'en',
            position: 'left',
            leadColor: '#c8c8c8',
            triggerColor: '#146ff8',
            triggerRadius: '50%',
            triggerPositionX: 'left',
            triggerPositionY: 'bottom',
            triggerIcon: 'wheel_chair4',
            triggerSize: 'medium',
            triggerOffsetX: 20,
            triggerOffsetY: 20,
            mobile: {
                triggerSize: 'small',
                triggerPositionX: 'left',
                triggerPositionY: 'center',
                triggerOffsetX: 0,
                triggerOffsetY: 0,
                triggerRadius: '0'
            }
        });
    };
}(document, 'script')); </script>";
}

add_action('fl_body_close', 'mmsession_custom_footer_js');
function mmsession_custom_footer_js() {
    
  echo "<script src='https://session.mm-api.agency/js/mmsession.js'></script>";
}

//chat meter Shortcode code start here

function chatmeter_function($arg)
{ 
    $website_json =  json_decode(get_option('website_json'));
     

     foreach($website_json->sites as $site_chat){

        if($site_chat->instance == 'prod'){

            return $site_chat->chatmeter;

        }
     } 
}

add_shortcode('chat_meter', 'chatmeter_function');

//chat meter Shortcode code end here

//Wells Fargo Financing link Shortcode code start here

function wells_fargo_function($arg)
{ 
    $website_json =  json_decode(get_option('website_json'));  
    
    $finlink = '';
     
    if($website_json->financeSync || $website_json->financeWF){
        
        if($website_json->financeWF){ $finlink = $website_json->financeWF;  }else{ $finlink = $website_json->financeSync; }        

        $content = '<a href="'.$finlink.'" target="_blank" class="fl-button financeSync" role="button" rel="noopener">
                         <span class="fl-button-text">'.$arg[0].'</span>
                    </a>';
                    
    }else{

        $content = '';

    }   

    return $content;
  
}

add_shortcode('wells_fargo_finance', 'wells_fargo_function');



function wells_fargo_function_link($arg)
{ 
    $website_json =  json_decode(get_option('website_json'));  
    
    $finlink = '';
     
    if($website_json->financeSync || $website_json->financeWF){
        
        if($website_json->financeWF){ 
            $finlink = $website_json->financeWF;  
        }else{ 
            $finlink = $website_json->financeSync; 
        }
                    
    }else{

        $finlink = '';

    }   

    return $finlink;
  
}

add_shortcode('wells_fargo_finance_link', 'wells_fargo_function_link');

//Synchrony Financing link Shortcode code start here

function synchrony_function($arg)
{ 
    $website_json =  json_decode(get_option('website_json'));    

    $finlink = '';

    if($website_json->financeSync || $website_json->financeWF){

        if($website_json->financeSync){ $finlink = $website_json->financeSync;  }else{ $finlink = $website_json->financeWF; }

        $content = '<a href="'.$website_json->financeSync.'" target="_blank" class="fl-button financeSync" role="button" rel="noopener">
                         <span class="fl-button-text">'.$arg[0].'</span>
                    </a>';

                    
    }else{

        $content = '';

    }

    return $content;

}

add_shortcode('synchrony_finance', 'synchrony_function');


function synchrony_function_link($arg)
{ 
    $website_json =  json_decode(get_option('website_json'));    

    $finlink = '';

    if($website_json->financeSync || $website_json->financeWF){

        if($website_json->financeSync){ 
            $finlink = $website_json->financeSync;  
        }else{ 
            $finlink = $website_json->financeWF;
         }       
                    
    }else{

        $finlink = '';

    }

    return $finlink;

}

add_shortcode('synchrony_finance_link', 'synchrony_function_link');

//Area rug affilate Button

function arearug_affiliate_button($arg){  
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";

    $loc_country = $website_json->locations[0]->country;
    if($loc_country =='US'){ $en = 'en_us'; }elseif($loc_country =='CA'){ $en = 'en_ca'; }

    if($rugAffiliateCode){

        $areabtn = '<div class="rugshopbtn-wrapper">';
        $areabtn .= '<a href="https://rugs.shop/'.$en.'/?store='.$rugAffiliateCode.'" target="_blank" class="fl-button rugshopbtn" role="button">
                            <span class="button-text">'.$arg[0].'</span></a>';
        $areabtn .= '</div>';

    }   
       return $areabtn;  
}

add_shortcode('rugshop_button', 'arearug_affiliate_button');

//Area rug affilate link
function arearug_affiliate_link($arg){  
    
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
    $loc_country = $website_json->locations[0]->country;
    if($loc_country =='US'){ $en = 'en_us'; }elseif($loc_country =='CA'){ $en = 'en_ca'; }

    if($rugAffiliateCode){

 $arealnk = '<a href="https://rugs.shop/'.$en.'/?store='.$rugAffiliateCode.'" target="_blank" class="rugshoplink">'.$arg[0].'</a>';

    }
   

       return $arealnk;

  
}

add_shortcode('rugshop_link', 'arearug_affiliate_link');

//Area rug affilate code
function arearug_affiliate_code($arg){  

        $website_json =  json_decode(get_option('website_json'));
        $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
    
        if($rugAffiliateCode){
    
            return $rugAffiliateCode;
    
        }
        else{

            return '';
        }   
      
    }    
add_shortcode('affiliate_code', 'arearug_affiliate_code');



//Check image path for swatch and gallery image

function get_image_url ($image,$height,$width){

    if(get_option('cloudinary',true) == 'false' || strpos($image, 'mmllc-images.s3') !== false || strpos($image, 'products.daltile.com') !== false || strpos($image, 's7.shawimg.com') !== false){

        
        if(strpos($image , 's7.shawimg.com') !== false){
            if(strpos($image , 'http') === false){ 
            $image = "http://" . $image;
        }	
           
        }else{
            if(strpos($image , 'http') === false){ 
            $image = "https://" . $image;
        }	
            $image= "https://mm-media-res.cloudinary.com/image/fetch/h_".$height.",w_".$width.",c_limit/".$image."";
        }
    
      }else{
    
        $image= 'https://res.cloudinary.com/mobilemarketing/image/upload/c_limit,h_'.$height.',w_'.$width.',q_auto'.$image;
    
      }	

      return $image;

}

//Single gallery image of product

function single_gallery_image_product($post_id,$height,$width){


    if(get_field('gallery_room_images', $post_id)){			
        
        // loop through the rows of data
        
        $gallery_image = get_field('gallery_room_images', $post_id);

		$gallery_i = explode("|",$gallery_image);
		

			foreach($gallery_i as  $key=>$value) {	
                
                
                $room_image =  get_image_url($value,'1000','1600');
               
                break;
			}
		  
    }

    return $room_image;
}

function thumb_gallery_images_in_plp_loop($value){

    return  get_plp_image_url($value,'222','222');    

}

function get_plp_image_url ($image,$height,$width){

    if(get_option('cloudinary',true) == 'false' || strpos($image, 'mmllc-images.s3') !== false || strpos($image, 'products.daltile.com') !== false || strpos($image, 's7.shawimg.com') !== false){

        
        if(strpos($image , 's7.shawimg.com') !== false){
            if(strpos($image , 'http') === false){ 
            $image = "http://" . $image;
        }	
           
        }else{
            if(strpos($image , 'http') === false){ 
            $image = "https://" . $image;
        }	
            $image= "https://mm-media-res.cloudinary.com/image/fetch/h_".$height.",w_".$width.",c_fill/".$image."";
        }
    
      }else{
    
        $image= 'https://res.cloudinary.com/mobilemarketing/image/upload/c_limit,h_'.$height.',w_'.$width.',q_auto'.$image;
    
      }	

      return $image;

}


//Cloudinary Swatch image high

function swatch_image_product($post_id,$height,$width){

$image = get_field('swatch_image_link',$post_id) ? get_field('swatch_image_link',$post_id):"http://placehold.it/168x123?text=No+Image"; 
return  get_image_url($image,$height,$width);

}

//Cloudinary Swatch image thumbnail 
function swatch_image_product_thumbnail($post_id,$height){


    $image = get_field('swatch_image_link',$post_id) ? get_field('swatch_image_link',$post_id):"http://placehold.it/168x123?text=No+Image"; 
    
    return  get_image_url($image,$height,'222');    
    
    }

//New PLP DB Query Cloudinary Swatch image thumbnail 
function newplp_swatch_image_product_thumbnail($image,$height){


    //$image = get_field('swatch_image_link',$post_id) ? get_field('swatch_image_link',$post_id):"http://placehold.it/168x123?text=No+Image"; 
    
    return  get_image_url($image,$height,'222');    
    
    }

//Cloudinary high resolution gallery image 
function high_gallery_images_in_loop($value){

    return  get_image_url($value,'1000','1600');  

}

//Cloudinary thumnail gallery image 
function thumb_gallery_images_in_loop($value){

    return  get_image_url($value,'222','222');    

}


///Seo optimization function according semrush audit report

/**
 * Dequeue the jQuery UI script.
 *
 * Hooked to the wp_print_scripts action, with a late priority (100),
 * so that it is after the script was enqueued.
 */
 function wpdocs_dequeue_script() {
    wp_dequeue_script( 'bb-header-footer' );
}
add_action( 'wp_print_scripts', 'wpdocs_dequeue_script', 100 );

/**
 * Dequeue BB header foooter css and added into grandchild styles.css
 */

function remove_bbhf_css() {   
    wp_dequeue_style( 'bbhf-style-css' );
 }
 add_action( 'wp_print_styles', 'remove_bbhf_css', 100 );

 /**
 * Removed emoji css and js
 */

 function disable_emoji_feature() {
	
	// Prevent Emoji from loading on the front-end
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );

	// Remove from admin area also
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	// Remove from RSS feeds also
	remove_filter( 'the_content_feed', 'wp_staticize_emoji');
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji');

	// Remove from Embeds
	remove_filter( 'embed_head', 'print_emoji_detection_script' );

	// Remove from emails
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

	// Disable from TinyMCE editor. Currently disabled in block editor by default
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );

	/** Finally, prevent character conversion too
         ** without this, emojis still work 
         ** if it is available on the user's device
	 */

	add_filter( 'option_use_smilies', '__return_false' );

}

function disable_emojis_tinymce( $plugins ) {
	if( is_array($plugins) ) {
		$plugins = array_diff( $plugins, array( 'wpemoji' ) );
	}
	return $plugins;
}

add_action('init', 'disable_emoji_feature');

 /**
 * //Remove JQuery migrate
 */
 
function remove_jquery_migrate( $scripts ) {
    if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) {
         $script = $scripts->registered['jquery'];
    if ( $script->deps ) { 
 // Check whether the script has any dependencies
 
         $script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
  }
  }
  }
 add_action( 'wp_default_scripts', 'remove_jquery_migrate' );

 //Custom link shortcode for blog post
function custom_link_shortcode($arg){    
   
    $page = url_to_postid($arg[0]);

    if($page){

        return $arg[0];
    }
    else{
        return '/';
      }       
  
}    
add_shortcode('Link', 'custom_link_shortcode');

//Covid home page banner hook
function bbtheme_covid_banner() {
    $iscovid  =  get_option('covid');
     if ( $iscovid == '1' &&  is_front_page() ) {

          $content = do_shortcode('[fl_builder_insert_layout slug="covid19-banner-row"]');
        
          echo $content;
     }
    
}
add_action( 'fl_content_open', 'bbtheme_covid_banner' );


//Roomvo script
add_action('fl_body_close', 'roomvo_custom_footer_js');
function roomvo_custom_footer_js() {

    $website_json_data = json_decode(get_option('website_json'));

    foreach($website_json_data->sites as $site_cloud){
            
        if($site_cloud->instance == 'prod'){
    
            if( $site_cloud->roomvo == 'true'){
    
  echo "<script src='https://www.roomvo.com/static/scripts/b2b/mobilemarketing.js' async></script>";

            }
        }
    }
}


//Roomvo script integration
function roomvo_script_integration($manufacturer,$sku,$pro_id){

    $website_json_data = json_decode(get_option('website_json'));

    foreach($website_json_data->sites as $site_cloud){
            
        if($site_cloud->instance == 'prod'){
    
            if( $site_cloud->roomvo == 'true'){

                    if($manufacturer == 'Bruce'){ 
                      $manufacturer = get_post_meta( $pro_id, 'brand', true );
                    }
                    elseif($manufacturer == 'AHF'){
                        $manufacturer = get_post_meta( $pro_id, 'brand', true );
                    }
                    elseif($manufacturer  == 'Shaw'){
                        $manufacturer = get_post_meta( $pro_id, 'brand', true );
                    }
                    else{
                        $manufacturer == $manufacturer ;
                    }

                    $manufacturer = str_replace(' ', '', strtolower($manufacturer));  

                    $content='<div id="roomvo">
                            <div class="roomvo-container">
                        <a class="roomvo-stimr button" data-sku="'.$sku.'" style="visibility: hidden;"><i class="fa fa-camera" aria-hidden="true"></i> &nbsp;SEE IN MY ROOM</a>
                            </div>
                    </div>';
       
               $content .='<script type="text/javascript">
                               function getProductSKU() {                       
                               return "'.$manufacturer.'-'.$sku.'";
                               }
                           </script>';
        
                 echo $content;
    
            }
    
        }
    }
   
}



if (!wp_next_scheduled('roomvo_csv_integration_cronjob')) {    
      
    wp_schedule_event( time() +  20800, 'daily', 'roomvo_csv_integration_cronjob');
}
add_action( 'roomvo_csv_integration_cronjob', 'roomvo_csv_integration' );


//Roomvo csv integration
function roomvo_csv_integration(){

    $data = array();
    
    $brandmapping = array(

        "carpeting",
        "hardwood_catalog",
        "laminate_catalog",
        "luxury_vinyl_tile",
        "tile_catalog"
        
    );    

$website_json_data = json_decode(get_option('website_json'));

foreach($website_json_data->sites as $site_cloud){
            
    if($site_cloud->instance == 'prod'){

        if( $site_cloud->roomvo == 'true'){

            $isroomvo = 'true';

        }
    }
}

            if($isroomvo == 'true'){

            $protocols = array('https://', 'https://www.', 'www.','http://', 'http://www.');
            $domain = str_replace($protocols, '', home_url());

            $upload_dir = wp_get_upload_dir();
            $file= $upload_dir['basedir']. '/sfn-data/product_file.csv';

            write_log($file);

            $file = fopen($file, 'w');
            
            // save the column headers
            fputcsv($file, array('Manufacturer', 'Manufacturer SKU Number', 'product page URL', 'Dealer name', 'Dealer website domain'));
            
            foreach($brandmapping as $product_post){

                write_log('<----------'.$product_post.' Started--------->');
                sleep(30);

            // Sample data. This can be fetched from mysql too

            global $wpdb;
            $product_table = $wpdb->prefix."posts";
            $products_data = $wpdb->get_results("SELECT ID FROM $product_table WHERE post_type = '$product_post' AND post_status = 'publish'");


                    $i =1;
                    foreach($products_data as $product) {

                     //   if(get_post_meta( $product->ID, 'manufacturer', true ) != 'Shaw'){ 

                                    if(get_post_meta( $product->ID, 'brand', true ) == 'Bruce'){ 
                                    $manufacturer = get_post_meta( $product->ID, 'brand', true );
                                    }
                                    elseif(get_post_meta( $product->ID, 'manufacturer', true ) == 'AHF'){
                                        $manufacturer = get_post_meta( $product->ID, 'brand', true );
                                    }
                                    elseif(get_post_meta( $product->ID, 'manufacturer', true ) == 'Shaw'){
                                        $manufacturer = get_post_meta( $product->ID, 'brand', true );
                                    }
                                    else{
                                        $manufacturer = get_post_meta( $product->ID, 'manufacturer', true );
                                    }

                                    $sku = get_post_meta( $product->ID, 'sku', true );
                                    $product_url = get_permalink( $product->ID );
                                    $dealer_name = $website_json_data->name;
                                    $domain_url = $domain; 
                                    $manufacturer = str_replace(' ', '', strtolower($manufacturer));

                                    fputcsv($file, array($manufacturer, $sku, $product_url, $dealer_name, $domain_url));

                                    if($i % 5000==0){ write_log($i.'----5000'); ob_flush(); sleep(10);}

                                    $i++;

                       // }
                        
                    }

            wp_reset_query();

            write_log('<----------'.$product_post.' Ended--------->');

            }
            
            $file_size =  $upload_dir['basedir']. '/sfn-data/product_file.csv' ;

            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.roomvo.com/pro/api/ext/update_dealer_mappings",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array(
                'requester' => 'mobilemarketing',
                'api_key' => 'cac7fb3c-7996-499d-8729-96a3547515b8',
                'dealer_name' => $website_json_data->name,
                'dealer_domain' => $domain_url,
                'email' => 'devteam.agency@gmail.com',
                'product_file' => new CURLFILE($file_size)
                ),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: multipart/form-data",
                "Content-Type: application/json"
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            write_log('<----------response--------->');
            write_log( $response);
            write_log('<----------response--------->');

            }else{

                write_log('<----------NO ROOMVO CDE SWITCH TRUE--------->');
            }
}

add_filter('the_excerpt', 'do_shortcode');
add_filter('get_the_excerpt', 'shortcode_unautop');
add_filter('get_the_excerpt', 'do_shortcode');

add_filter( 'facetwp_per_page_options', function( $options ) {
    return array( 12,24,36,48,60 );
});

/*Thank you page image*/
function thankyou_responseimage(){

    $promos = json_decode(get_option('promos_json'));

    foreach($promos as $promo){

                foreach($promo->widgets as $widget){                    

                    if($widget->type =='thanks'){

                        $realArray = (array)$widget;                      
                        $realArray1 = (array)$realArray['images'][0];
                        $realArray2 = (array)$realArray['images'][01];

                       // $thanks =  array($promo->promoCode => $realArray1['url']);
                        $thanks =  array( $realArray1['url'], $realArray2['url']);

                    }

                }

    }

    if($_GET['promocode'] !=''){       

        $content = '<img class="fl-photo-img thankuimage-desktop" src="https://'.$thanks[0].'" alt="thank-you-image"  title="thank_you_image">
        <img class="fl-photo-img thankuimage-mobile" src="https://'.$thanks[1].'" alt="thank-you-image"  title="thank_you_image">';

        return $content;
    }
}
add_shortcode( 'thankyou-responseimage', 'thankyou_responseimage' );


function get_custom_post_type_template($single_template) {
    global $post;
      
    if ($post->post_type != 'post') {
         $single_template = get_stylesheet_directory() . '/product-listing-templates/single-'.$post->post_type.'.php';
    }
    return $single_template;
}

add_filter( 'single_template', 'get_custom_post_type_template' );


function  query_group_by_filter($groupby){
    global $wpdb;

    return $wpdb->postmeta . '.meta_value ';
 }

add_filter( 'facetwp_search_query_args', function( $search_args, $params ) {
    remove_filter('posts_groupby', 'query_group_by_filter'); 
    return $search_args;
}, 10, 2 );

add_filter( 'facetwp_result_count', function( $output, $params ) {
    $output = $params["lower"]."-".$params["upper"]." of ".$params['total'] .' Results';
    return $output;
}, 10, 2 );


add_filter( 'facetwp_facet_render_args', function( $args) {
    global $post;
	if ( 'search' == $args['facet']['name'] ) { 
        $current = $post->ID;
        $parent = $post->post_parent;
		$args['facet']['placeholder'] = 'Search '.get_the_title($parent);
	}
	return $args;
} );

add_filter( 'facetwp_template_html', function( $output, $class ) {
    //$GLOBALS['wp_query'] = $class->query;
    $prod_list = $class->query;
    
   ob_start();
    

   if( get_option('plplayout') !='' && get_option('plplayout') == '1' ){

        $dir =  get_stylesheet_directory().'/product-listing-templates/product-loop-popup.php';
        require_once $dir;
   }else{
        $dir =  get_stylesheet_directory().'/product-listing-templates/product-loop-new.php';
        require_once $dir;
   }  
    
    return ob_get_clean();
}, 10, 2 );

add_filter( 'facetwp_query_args', function( $query_args, $class ) {
    // if ( 'luxury_vinyl_tile' == $class->ajax_params['template'] ) {
        $query_args['posts_per_page']='12';
        $query_args['meta_key']='collection';
        if(($query_args['post_type'] !="fl-builder-template" && $query_args['post_type'] !="post") &&  @wp_count_posts($query_args['post_type'])->publish > 1 ){
            add_filter('posts_groupby', 'query_group_by_filter');
        }
    // }
    return $query_args;
}, 10, 2 );